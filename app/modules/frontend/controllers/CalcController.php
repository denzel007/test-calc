<?php

namespace Calc\Modules\Frontend\Controllers;

class CalcController extends ControllerBase
{
    
    public function indexAction()
    {
        if ($this->request->isPost()) {
            $expression = $this->request->getPost('expression', ["string","striptags","trim"]);
            chdir ( BASE_PATH );
            #cli command use here
            $res =  exec('php run calc main "'.$expression.'"');
            $resExpl = explode('result:',$res);
            if (isset($resExpl[1])) {
                return $resExpl[1];
            } else {
               return 'error'; 
            }
            exit;
        }
        echo 'error';exit;
    }        

}
