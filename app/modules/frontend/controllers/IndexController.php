<?php

namespace Calc\Modules\Frontend\Controllers;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        
    }
    
    public function calcAction()
    {
        if ($this->request->isPost()) {
            $expression = $this->request->getPost('expression', ["string","striptags","trim"]);
            chdir ( BASE_PATH );
            $res =  exec("php run calc main ".$expression."");
            $resExpl = explode('result:',$res);
            if (isset($resExpl[1])) {
                return $resExpl[1];
            } else {
               return 'error'; 
            }
            exit;
        }
        echo 'error';exit;
    }        

}

