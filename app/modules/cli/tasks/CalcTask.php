<?php
namespace Calc\Modules\Cli\Tasks;

/* 
 * Calc\Modules\Cli\Tasks\CalcTask;
 * 
 * run example 1 (with vars): php run calc main "a=2+2;b=3-1;a+b+3"
 * run example 2 (without vars): php run calc main "2+2*2"
 * run example 3 (with vars): php run calc main "f=1+1;c=2;b=f+c;1+b+c+f*2"
 * run example 4 (input by lines): php run calc OR php run calc main
*/

class CalcTask extends \Phalcon\Cli\Task
{
    
    private $vars = [];
    
    public function mainAction($paramsArr)
    {
        $commands = []; $paramsPrepare = [];
        
        #use ";" as command separator
        if (isset($paramsArr['params_split'])) {
         $paramsPrepare = explode(';',$paramsArr['params_split']);
        }
        
        #delete empty values
        $paramsP = array_filter($paramsPrepare);
        
        # run without params  
        if (empty($paramsP)) {
                
                $stdin = fopen('php://stdin', 'r');
                $yes   = false;
                
                while (!$yes)
                {
                        echo PHP_EOL.' type next expression or "exit" '.PHP_EOL;

                        $input = trim(fgets($stdin));

                        if ($input == 'exit')
                        {
                            break;
                        } else {
                             $command = $input; 
                             $commands[] = $input;
                        }
                     
                     if (count($commands)>0) {
                       $paramsP =  $command;
                      }
                     
                     $paramsPrepare = explode(';',$paramsP);
        
                     #delete empty values
                     $paramsP = array_filter($paramsPrepare);  
                     $this->calc($paramsP);   
                }
                
        }
        
        if (!empty($paramsP)) {
         $this->calc($paramsP);
        } else {
            echo 'bye.';
        }
        
    }
    
    
    private function calc($paramsP)
    {
        #delete spaces
        foreach ($paramsP as &$paramP) {
            $paramP = preg_replace('/\s+/', '', $paramP);  
        }
        
        #init vars
        //$vars = []; 
        $expression = '';
        $varsString = '';
        
        #catch errors
        try{

            #if expression with vars, for example: "a=2+2;b=3-1;a+b+3"
            if (count($paramsP)>1) {

                foreach ($paramsP as $param) {

                     # if exist "var=..." expression
                     $explVar = explode('=',$param);
                     
                     #if exist new var
                     if(isset($explVar[1]) AND ctype_alpha($explVar[0]) AND !array_key_exists($explVar[0], $this->vars)) {
                          
                          #echo PHP_EOL.'key: '.$explVar[0].'; val: '.$explVar[1];
                          
                          #find old vars begin
                          $onlyVars = preg_replace("/[^a-zA-Z]+/", "", $explVar[1]);
                          
                          $arr = str_split($onlyVars);
                          
                          $count = 0;
                          foreach ($arr as $valueA) {
                             if (array_key_exists($valueA, $this->vars)) {
                                 $count++;
                             } 
                          }
                          #find old vars end
                            
                          if ($count>=2) {
                             $this->vars[$explVar[0]] = math_eval($explVar[1], $this->vars); 
                          } else {
                              $this->vars[$explVar[0]] = math_eval($explVar[1]); 
                          }

                     # if exist only expression   
                     } elseif(!isset($explVar[1])) {
                         $expression = $param;
                     } 
                     
                }
                     #print_r($explVar);print_r($vars);print_r($expression);exit;
                     
                     
                     if (!empty($this->vars) AND !empty($expression)) {
                         
                     $r = math_eval($expression,$this->vars);
                     
                         foreach ($this->vars as $keyV => $valueV) {
                             $varsString = "$keyV=$valueV; ".$varsString;
                         }
                         
                         echo PHP_EOL.' vars: '.$varsString.'expression: '.$expression.'; result:'.$r;
                         
                     } else {
                        echo PHP_EOL.' error: Wrong command syntax! Try "a=2+2;b=3-1;a+b+3" or "2+2*2" or "f=1+1;c=2;b=f+c;1+b+c+f*2"';  
                     }

            # if expression without vars, for example: "2+2" or ""2+2*2^2" 
            } else {
                
                 if (!empty($this->vars)) {  
                     $r = math_eval($paramsP[0],$this->vars);
                     foreach ($this->vars as $keyV => $valueV) {
                             $varsString = "$keyV=$valueV; ".$varsString;
                         }
                     echo PHP_EOL.' vars: '.$varsString.' expression: '.$paramsP[0].'; result:'.$r;
                 } else {    
                    $r = math_eval($paramsP[0]);
                    echo PHP_EOL.' expression: '.$paramsP[0].'; result:'.$r;
                 }
               

            }
       
       #print errors     
       } catch(\Exception $e){
           
           echo PHP_EOL.' error: '.$e->getMessage().'; Try "a=2+2;b=3-1;a+b+3" or "2+2*2" or "f=1+1;c=2;b=f+c;1+b+c+f*2"';  
           
       }
    }        
    
}
