<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * Register Namespaces
 */
$loader->registerNamespaces([
    'Calc\Models' => APP_PATH . '/common/models/',
    'Calc'        => APP_PATH . '/common/library/',
]);

/**
 * Register module classes
 */
$loader->registerClasses([
    'Calc\Modules\Frontend\Module' => APP_PATH . '/modules/frontend/Module.php',
    'Calc\Modules\Cli\Module'      => APP_PATH . '/modules/cli/Module.php'
]);

$loader->register();
